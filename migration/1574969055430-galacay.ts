import {MigrationInterface, QueryRunner} from "typeorm";

export class galacay1574969055430 implements MigrationInterface {
    name = 'galacay1574969055430'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "tags" ("id" SERIAL NOT NULL, "name" character varying(45) NOT NULL, "created" TIMESTAMP NOT NULL DEFAULT now(), "updated" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_e7dc17249a1148a1970748eda99" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "posts" ("id" SERIAL NOT NULL, "texto" character varying(45) NOT NULL, "created" TIMESTAMP NOT NULL DEFAULT now(), "updated" TIMESTAMP NOT NULL DEFAULT now(), "fk_user_id" integer NOT NULL, CONSTRAINT "PK_2829ac61eff60fcec60d7274b9e" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE INDEX "fk_posts_1_idx" ON "posts" ("fk_user_id") `, undefined);
        await queryRunner.query(`CREATE TABLE "users" ("id" SERIAL NOT NULL, "name" character varying(45) NOT NULL, "surname" character varying(45) NOT NULL, "birthdate" date NOT NULL, "created" TIMESTAMP NOT NULL DEFAULT now(), "updated" TIMESTAMP NOT NULL DEFAULT now(), "fk_group_id" integer NOT NULL, CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "groups" ("id" SERIAL NOT NULL, "name" character varying(45) NOT NULL, "description" character varying(200) NOT NULL, "created" TIMESTAMP NOT NULL DEFAULT now(), "updated" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_659d1483316afb28afd3a90646e" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "post_tag" ("fk_post_id" integer NOT NULL, "fk_tag_id" integer NOT NULL, CONSTRAINT "PK_19623260812633cc2c662b84a6c" PRIMARY KEY ("fk_post_id", "fk_tag_id"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_18cf48f94543715235ba85bc44" ON "post_tag" ("fk_post_id") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_3f341054a5beb0115f77ec961a" ON "post_tag" ("fk_tag_id") `, undefined);
        await queryRunner.query(`ALTER TABLE "posts" ADD CONSTRAINT "FK_bd6ea82e0287a36bb1fa009e359" FOREIGN KEY ("fk_user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "FK_d6323405385edfeeda9732b5248" FOREIGN KEY ("fk_group_id") REFERENCES "groups"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "post_tag" ADD CONSTRAINT "FK_18cf48f94543715235ba85bc441" FOREIGN KEY ("fk_post_id") REFERENCES "posts"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "post_tag" ADD CONSTRAINT "FK_3f341054a5beb0115f77ec961a3" FOREIGN KEY ("fk_tag_id") REFERENCES "tags"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);

        await queryRunner.query(`INSERT INTO groups (id, name, description) VALUES
            (1, 'Admin', 'admin del sistema'),
            (2, 'Users', 'usuarios del sistema'),
            (3, 'Contenido', 'Usuarios que administran el contenido del sistema'),
            (4, 'nea', 'nae'),
            (5, 'uno más', 'uno más'),
            (6, 'grupo1', 'grupo1'),
            (7, 'naaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'admin del ssssssssssssss')`, undefined);

        await queryRunner.query(`INSERT INTO users (id, name, surname, birthdate, fk_group_id) VALUES
        (1, 'Rodrigo', 'Rivero', '1991-04-01 20:00:00', 1),
        (2, 'Andrés', 'Sáez', '1991-04-01 20:00:00', 1),
        (3, 'Felipe', 'Olavarría', '1991-04-01 20:00:00', 2),
        (4, 'Rodrigo', 'Rivero', '1991-02-04 00:00:00', 2),
        (5, 'Jc', 'Mc', '1966-12-22 00:00:00', 2),
        (6, 'Fr', 'Vá', '1994-09-14 00:00:00', 2),
        (9, 'ryu', 'hayabusa', '1984-08-24 00:00:00', 2),
        (10, 'Niuna', 'Weá', '1991-04-02 00:00:00', 2),
        (11, 'Rodrigo', 'Rivero', '2019-09-24 00:00:00', 2),
        (12, 'Netlify', 'Rocks', '1991-04-02 04:00:00', 2),
        (13, 'Rodrigo', 'Rivero', '2019-09-24 00:00:00', 2),
        (14, 'Rodrigo', 'Rivero', '2019-09-24 00:00:00', 2),
        (15, 'Rodrigo', 'Rivero', '2019-09-30 00:00:00', 2),
        (16, 'Rodrigo', 'Rivero', '2019-09-24 00:00:00', 2),
        (17, 'Rodrigo', 'Rivero', '2019-09-24 00:00:00', 2),
        (18, 'Peláenz', 'empresas', '2019-06-23 00:00:00', 2),
        (19, 'Rodrigo', 'Rivero', '2019-09-30 00:00:00', 2),
        (20, 'nadita', 'nadita', '2019-09-29 00:00:00', 2),
        (21, 'Rodrigo Andrés', 'Rivero Sáez', '1991-04-02 00:00:00', 2),
        (22, 'Andrés Rodrigo', 'Sáez Rivero', '1991-04-02 00:00:00', 2),
        (27, 'niuna', 'weá', '2019-09-25 00:00:00', 2),
        (28, 'probando', 'add front', '2019-09-25 00:00:00', 2),
        (29, 'añadiendo', 'desde el front', '2019-09-25 00:00:00', 2),
        (30, 'adding', 'fromFront', '2019-09-25 00:00:00', 2),
        (32, 'na', 'na', '2019-09-25 00:00:00', 2),
        (33, 'First', 'Last', '2019-09-25 00:00:00', 2),
        (58, 'Desdel', 'Celu', '2019-09-25 03:00:00', 2),
        (59, 'Nae', 'Nae', '2019-09-25 03:00:00', 2),
        (61, 'Denys', 'Alimaña', '2019-09-25 03:00:00', 1),
        (63, 'admin', 'admin', '2019-09-28 00:00:00', 1),
        (64, 'user', 'user', '2019-09-28 00:00:00', 2),
        (65, 'a', 'a', '2019-09-28 00:00:00', 1),
        (66, 'b', 'b', '2019-09-28 00:00:00', 1),
        (67, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (68, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (69, 'borra', 'borra', '2019-09-30 00:00:00', 1),
        (70, 'borra', 'borra', '2019-09-30 00:00:00', 1),
        (71, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (72, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (73, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (74, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (75, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (76, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (77, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (78, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (79, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (80, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (81, 'ahora sí', 'ahora sí', '2019-09-30 00:00:00', 1),
        (82, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (83, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (84, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (85, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (86, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (87, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (88, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (89, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (90, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (91, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (92, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (93, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (94, 'borraaa', 'borra', '2019-09-30 00:00:00', 2),
        (95, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (96, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (97, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (98, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (99, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (100, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (101, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (102, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (103, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (104, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (105, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (106, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (107, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (109, 'borra', 'borra', '2019-09-30 00:00:00', 2),
        (110, 'borra', 'borra', '2019-09-30 00:00:00', 1),
        (111, 'borra', 'borra', '2019-09-30 00:00:00', 1),
        (112, 'Rodrigo', 'Rivero', '2019-09-30 00:00:00', 1),
        (113, 'defff', 'defff', '2019-09-23 00:00:00', 1),
        (114, 'na', 'na', '2019-09-30 00:00:00', 2),
        (115, 'Desde', 'Nuevo', '2019-10-14 03:00:00', 2),
        (116, 'na', 'na', '2019-10-30 03:00:00', 2),
        (117, 'desde pc', 'desde pc', '2019-11-18 03:00:00', 2),
        (118, 'Rhh', 'Fvf', '2019-11-13 03:00:00', 2)`, undefined);

        await queryRunner.query(`INSERT INTO posts (id, texto, fk_user_id) VALUES
            (1, 'Mi primer post de prueba', 1),
            (2, '2do actualizado por segunda vez', 1),
            (3, 'Haciendo un post con otro usuario', 2),
            (4, 'Post con tipo usuario', 3)`, undefined);

        await queryRunner.query(`INSERT INTO tags (id, name) VALUES
            (1, 'Informática'),
            (2, 'aaaaaaaaaaaaa'),
            (3, 'Cualquier weá'),
            (4, 'Niuna weá'),
            (5, 'De todo'),
            (6, 'Electrónica'),
            (7, 'Ocio'),
            (8, 'Aburrimiento'),
            (9, 'Terror'),
            (10, 'Acción')`, undefined);

        await queryRunner.query(`INSERT INTO post_tag (fk_post_id, fk_tag_id) VALUES
            (1, 1),
            (2, 1),
            (2, 2),
            (2, 7),
            (2, 8),
            (2, 9),
            (2, 10),
            (3, 1)`, undefined);

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "post_tag" DROP CONSTRAINT "FK_3f341054a5beb0115f77ec961a3"`, undefined);
        await queryRunner.query(`ALTER TABLE "post_tag" DROP CONSTRAINT "FK_18cf48f94543715235ba85bc441"`, undefined);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "FK_d6323405385edfeeda9732b5248"`, undefined);
        await queryRunner.query(`ALTER TABLE "posts" DROP CONSTRAINT "FK_bd6ea82e0287a36bb1fa009e359"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_3f341054a5beb0115f77ec961a"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_18cf48f94543715235ba85bc44"`, undefined);
        await queryRunner.query(`DROP TABLE "post_tag"`, undefined);
        await queryRunner.query(`DROP TABLE "groups"`, undefined);
        await queryRunner.query(`DROP TABLE "users"`, undefined);
        await queryRunner.query(`DROP INDEX "fk_posts_1_idx"`, undefined);
        await queryRunner.query(`DROP TABLE "posts"`, undefined);
        await queryRunner.query(`DROP TABLE "tags"`, undefined);
    }

}
