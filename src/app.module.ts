import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GroupModule } from './group/group.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { TagModule } from './tag/tag.module';
import { PostModule } from './post/post.module';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'postgres',
    host: 'salt.db.elephantsql.com',
    port: 5432,
    username: 'nazmeyqv',
    password: 'JCPvKJo6OB1ohXShArY0_V7qWrXDyA92',
    database: 'nazmeyqv',
    entities: ['**/*.entity.js'],
    logging: true
  }),
    GroupModule,
    UserModule,
    TagModule,
    PostModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
