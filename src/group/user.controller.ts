import { Controller, HttpException } from '@nestjs/common';
import { UserService } from '../user/user.service';

import {
  ApiBearerAuth, ApiUseTags,
} from '@nestjs/swagger';
import { User } from '../user/user.entity';
import { Crud, CrudController, Override, ParsedRequest, CrudRequest, ParsedBody } from '@nestjsx/crud';
import { Group } from './group.entity';
import { GroupService } from './group.service';

@ApiBearerAuth()
@Crud({
  model: {
    type: User,
  },
  params: {
    groupId: {
      field: 'group',
      type: 'number'
    }
  },
  query: {
    join: {
      group: {}
    }
  }
})
@ApiUseTags('groups')
@Controller('/groups/:groupId/users')
export class UserController {
  constructor(public service: UserService,
              public groupService: GroupService) {}

  get base(): CrudController<User> {
    return this;
  }

  @Override()
  async createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: User,
  ) {

    let group: Group;

    for (const filter of req.parsed.paramsFilter) {
      group = await this.groupService.findOne(filter.value);
    }
    if (!group) {
      throw new HttpException('Group not found', 404);
    }
    dto.group = group;
    req.parsed.paramsFilter = [];
    return  this.base.createOneBase(req, dto);
  }
}
